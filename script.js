
function main() {
test
	var MAP = `{
   "name": "Futsal 3x3 Lucas",
   "width": 755,
   "height": 339,
   "spawnDistance": 310,
   "bg": {
      "type": "hockey",
      "width": 665,
      "height": 290,
      "kickOffRadius": 80,
      "cornerRadius": 0
   },
   "vertexes": [
      {
         "x": -665,
         "y": 290,
         "trait": "ballArea",
         "cMask": [
            "ball"
         ],
         "bCoef": 1
      },
      {
         "x": -665,
         "y": 80,
         "trait": "ballArea",
         "cMask": [
            "ball"
         ],
         "bCoef": 1

      },
      {
         "x": -665,
         "y": -80,
         "trait": "ballArea",
         "cMask": [
            "ball"
         ],
         "bCoef": 1
      },
      {
         "x": -665,
         "y": -290,
         "trait": "ballArea",
         "bCoef": 1,
         "cMask": [
            "ball"
         ]
      },
      {
         "x": 665,
         "y": 290,
         "trait": "ballArea",
         "cMask": [
            "ball"
         ],
         "bCoef": 1
      },
      {
         "x": 665,
         "y": 80,
         "trait": "ballArea",
         "cMask": [
            "ball"
         ],
         "bCoef": 1
      },
      {
         "x": 665,
         "y": -80,
         "trait": "ballArea",
         "cMask": [
            "ball"
         ],
         "bCoef": 1
      },
      {
         "x": 665,
         "y": -290,
         "trait": "ballArea",
         "cMask": [
            "ball"
         ],
         "bCoef": 1
      },
      {
         "x": 0,
         "y": 306,
         "trait": "kickOffBarrier"
      },
      {
         "x": 0,
         "y": 80,
         "trait": "kickOffBarrier"
      },
      {
         "x": 0,
         "y": -80,
         "trait": "line"
      },
      {
         "x": 0,
         "y": -306,
         "trait": "kickOffBarrier"
      },
      {
         "bCoef": 0.1,
         "cMask": [
            "ball"
         ],
         "trait": "goalNet",
         "x": -693,
         "y": -80
      },
      {
         "bCoef": 0.1,
         "cMask": [
            "ball"
         ],
         "trait": "goalNet",
         "x": 693,
         "y": -80
      },
      {
         "bCoef": 0.1,
         "cMask": [
            "ball"
         ],
         "trait": "goalNet",
         "x": -693,
         "y": 80
      },
      {
         "bCoef": 0.1,
         "cMask": [
            "ball"
         ],
         "trait": "goalNet",
         "x": 693,
         "y": 80
      },
      {
         "trait": "line",
         "x": -665,
         "y": -215
      },
      {
         "trait": "line",
         "x": -500,
         "y": -50
      },
      {
         "trait": "line",
         "x": 665,
         "y": -215
      },
      {
         "trait": "line",
         "x": 500,
         "y": -50
      },
      {
         "trait": "line",
         "x": -665,
         "y": 215
      },
      {
         "trait": "line",
         "x": -500,
         "y": 50
      },
      {
         "trait": "line",
         "x": 665,
         "y": 215
      },
      {
         "trait": "line",
         "x": 500,
         "y": 50
      },
      {
         "bCoef": 1,
         "trait": "ballArea",
         "x": 665,
         "y": 290
      },
      {
         "bCoef": 1,
         "trait": "ballArea",
         "x": 665,
         "y": -290
      },
      {
         "bCoef": 0,
         "trait": "line",
         "x": 0,
         "y": 290
      },
      {
         "bCoef": 0,
         "trait": "line",
         "x": 0,
         "y": -290
      },
      {
         "x": 0,
         "y": 80,
         "trait": "kickOffBarrier"
      },
      {
         "x": 0,
         "y": -80,
         "trait": "kickOffBarrier"
      },
      {
         "x": 674,
         "y": -80,
         "trait": "line",
         "cMask": [
            "ball"
         ],
         "bCoef": 1
      },
      {
         "x": 674,
         "y": -290,
         "trait": "ballArea",
         "cMask": [
            "ball"
         ],
         "bCoef": 1
      },
      {
         "x": -674,
         "y": -80,
         "trait": "line",
         "cMask": [
            "ball"
         ],
         "bCoef": 1
      },
      {
         "x": -674,
         "y": -290,
         "trait": "ballArea",
         "cMask": [
            "ball"
         ],
         "bCoef": 1
      },
      {
         "x": -674,
         "y": 80,
         "trait": "line",
         "cMask": [
            "ball"
         ],
         "bCoef": 1
      },
      {
         "x": -674,
         "y": 290,
         "trait": "ballArea",
         "cMask": [
            "ball"
         ],
         "bCoef": 1
      },
      {
         "x": 674,
         "y": 80,
         "trait": "line",
         "cMask": [
            "ball"
         ],
         "bCoef": 1
      },
      {
         "x": 674,
         "y": 290,
         "trait": "ballArea",
         "cMask": [
            "ball"
         ],
         "bCoef": 1
      }
   ],
   "segments": [
      {
         "v0": 0,
         "v1": 1,
         "trait": "ballArea"
      },
      {
         "v0": 2,
         "v1": 3,
         "trait": "ballArea"
      },
      {
         "v0": 4,
         "v1": 5,
         "trait": "ballArea"
      },
      {
         "v0": 6,
         "v1": 7,
         "trait": "ballArea"
      },
      {
         "v0": 8,
         "v1": 9,
         "trait": "kickOffBarrier"
      },
      {
         "v0": 9,
         "v1": 10,
         "trait": "kickOffBarrier",
         "curve": 180,
         "cGroup": [
            "blueKO"
         ]
      },
      {
         "v0": 9,
         "v1": 10,
         "trait": "kickOffBarrier",
         "curve": -180,
         "cGroup": [
            "redKO"
         ]
      },
      {
         "v0": 10,
         "v1": 11,
         "trait": "kickOffBarrier"
      },
      {
         "vis": true,
         "bCoef": 0.1,
         "cMask": [
            "ball"
         ],
         "trait": "goalNet",
         "v0": 2,
         "v1": 12,
         "color": "FFFFFF",
         "curve": -35
      },
      {
         "vis": true,
         "bCoef": 0.1,
         "cMask": [
            "ball"
         ],
         "trait": "goalNet",
         "v0": 6,
         "v1": 13,
         "color": "FFFFFF",
         "curve": 35
      },
      {
         "vis": true,
         "bCoef": 0.1,
         "cMask": [
            "ball"
         ],
         "trait": "goalNet",
         "v0": 1,
         "v1": 14,
         "color": "FFFFFF",
         "curve": 35
      },
      {
         "vis": true,
         "bCoef": 0.1,
         "cMask": [
            "ball"
         ],
         "trait": "goalNet",
         "v0": 5,
         "v1": 15,
         "color": "FFFFFF",
         "curve": -35
      },
      {
         "vis": true,
         "bCoef": 0.1,
         "cMask": [
            "ball"
         ],
         "trait": "goalNet",
         "v0": 12,
         "v1": 14,
         "x": -585,
         "color": "FFFFFF",
         "curve": -35
      },
      {
         "vis": true,
         "bCoef": 0.1,
         "cMask": [
            "ball"
         ],
         "trait": "goalNet",
         "v0": 13,
         "v1": 15,
         "x": 585,
         "color": "FFFFFF",
         "curve": 35
      },
      {
         "color": "FFFFFF",
         "trait": "line",
         "v0": 16,
         "v1": 17,
         "curve": 90
      },
      {
         "color": "FFFFFF",
         "trait": "line",
         "v0": 18,
         "v1": 19,
         "curve": -90
      },
      {
         "color": "FFFFFF",
         "trait": "line",
         "v0": 20,
         "v1": 21,
         "curve": -90
      },
      {
         "color": "FFFFFF",
         "trait": "line",
         "v0": 22,
         "v1": 23,
         "curve": 90
      },
      {
         "vis": true,
         "color": "FFFFFF",
         "bCoef": 0,
         "trait": "line",
         "v0": 17,
         "v1": 21,
         "curve": 0
      },
      {
         "vis": true,
         "color": "FFFFFF",
         "bCoef": 0,
         "trait": "line",
         "v0": 19,
         "v1": 23,
         "curve": 0
      },
      {
         "vis": true,
         "color": "FFFFFF",
         "bCoef": 1,
         "trait": "ballArea",
         "v0": 1,
         "v1": 0,
         "cMask": [
            "ball"
         ],
         "x": -665
      },
      {
         "vis": true,
         "color": "FFFFFF",
         "bCoef": 1,
         "trait": "ballArea",
         "v0": 5,
         "v1": 4,
         "cMask": [
            "ball"
         ],
         "x": 665
      },
      {
         "vis": true,
         "color": "FFFFFF",
         "bCoef": 1,
         "trait": "ballArea",
         "v0": 2,
         "v1": 3,
         "cMask": [
            "ball"
         ],
         "x": -665
      },
      {
         "vis": true,
         "color": "FFFFFF",
         "bCoef": 1,
         "trait": "ballArea",
         "v0": 6,
         "v1": 7,
         "cMask": [
            "ball"
         ],
         "x": 665
      },
      {
         "vis": true,
         "color": "FFFFFF",
         "bCoef": 1,
         "trait": "ballArea",
         "v0": 0,
         "v1": 24,
         "y": 290
      },
      {
         "vis": true,
         "color": "FFFFFF",
         "bCoef": 1,
         "trait": "ballArea",
         "v0": 3,
         "v1": 25,
         "y": -290
      },
      {
         "curve": 0,
         "vis": true,
         "color": "FFFFFF",
         "bCoef": 0,
         "trait": "line",
         "v0": 26,
         "v1": 27
      },
      {
         "curve": -180,
         "vis": true,
         "color": "FFFFFF",
         "bCoef": 0,
         "trait": "line",
         "v0": 10,
         "v1": 9
      },
      {
         "curve": 180,
         "vis": true,
         "color": "FFFFFF",
         "bCoef": 0,
         "trait": "line",
         "v0": 29,
         "v1": 28
      },
      {
         "curve": 0,
         "vis": true,
         "color": "FFFFFF",
         "bCoef": 0,
         "trait": "line",
         "v0": 2,
         "v1": 1
      },
      {
         "curve": 0,
         "vis": true,
         "color": "FFFFFF",
         "bCoef": 0,
         "trait": "line",
         "v0": 6,
         "v1": 5
      },
      {
         "vis": false,
         "color": "FFFFFF",
         "bCoef": 1,
         "trait": "ballArea",
         "v0": 30,
         "v1": 31,
         "cMask": [
            "ball"
         ],
         "x": 614
      },
      {
         "vis": false,
         "color": "FFFFFF",
         "bCoef": 1,
         "trait": "ballArea",
         "v0": 32,
         "v1": 33,
         "cMask": [
            "ball"
         ],
         "x": -614
      },
      {
         "vis": false,
         "color": "FFFFFF",
         "bCoef": 1,
         "trait": "ballArea",
         "v0": 34,
         "v1": 35,
         "cMask": [
            "ball"
         ],
         "x": -614
      },
      {
         "vis": false,
         "color": "FFFFFF",
         "bCoef": 1,
         "trait": "ballArea",
         "v0": 36,
         "v1": 37,
         "cMask": [
            "ball"
         ],
         "x": 614
      }
   ],
   "goals": [
      {
         "p0": [
            -674,
            -80
         ],
         "p1": [
            -674,
            80
         ],
         "team": "red"
      },
      {
         "p0": [
            674,
            80
         ],
         "p1": [
            674,
            -80
         ],
         "team": "blue"
      }
   ],
   "discs": [
      {
         "pos": [
            -665,
            80
         ],
         "trait": "goalPost",
         "color": "FFFFFF",
         "radius": 5
      },
      {
         "pos": [
            -665,
            -80
         ],
         "trait": "goalPost",
         "color": "FFFFFF",
         "radius": 5
      },
      {
         "pos": [
            665,
            80
         ],
         "trait": "goalPost",
         "color": "FFFFFF",
         "radius": 5
      },
      {
         "pos": [
            665,
            -80
         ],
         "trait": "goalPost",
         "color": "FFFFFF",
         "radius": 5
      }
   ],
   "planes": [
      {
         "normal": [
            0,
            1
         ],
         "dist": -290,
         "trait": "ballArea"
      },
      {
         "normal": [
            0,
            -1
         ],
         "dist": -290,
         "trait": "ballArea"
      },
      {
         "normal": [
            0,
            1
         ],
         "dist": -339,
         "bCoef": 0.2,
         "cMask": [
            "all"
         ]
      },
      {
         "normal": [
            0,
            -1
         ],
         "dist": -339,
         "bCoef": 0.2,
         "cMask": [
            "all"
         ]
      },
      {
         "normal": [
            1,
            0
         ],
         "dist": -755,
         "bCoef": 0.2,
         "cMask": [
            "all"
         ]
      },
      {
         "normal": [
            -1,
            0
         ],
         "dist": -755,
         "bCoef": 0.2,
         "cMask": [
            "all"
         ]
      }
   ],
   "traits": {
      "ballArea": {
         "vis": false,
         "bCoef": 1,
         "cMask": [
            "ball"
         ]
      },
      "goalPost": {
         "radius": 8,
         "invMass": 0,
         "bCoef": 1
      },
      "goalNet": {
         "vis": true,
         "bCoef": 0.1,
         "cMask": [
            "all"
         ]
      },
      "kickOffBarrier": {
         "vis": false,
         "bCoef": 0.1,
         "cGroup": [
            "redKO",
            "blueKO"
         ],
         "cMask": [
            "red",
            "blue"
         ]
      },
      "line": {
         "vis": true,
         "bCoef": 0,
         "cMask": [
            ""
         ]
      },
      "arco": {
         "radius": 2,
         "cMask": [
            "n/d"
         ],
         "color": "cccccc"
      }
   },
   "playerPhysics": {
      "acceleration": 0.11,
      "kickingAcceleration": 0.1,
      "kickStrength": 7
   },
   "ballPhysics": {
      "radius": 6.4,
      "color": "EAFF00"
   }
}`
var ROOM_NAME = "Futsal 2v2 3v3 Lucas WS";
var ROOM_MAX_PLAYERS = 10;
var ROOM_PUBLIC = false;
var ADMIN_NAME = "admin";
var RED_TEAM = 1;
var BLUE_TEAM = 2;
var SPECTATOR = 0;

var room = HBInit({roomName: ROOM_NAME, maxPlayers: ROOM_MAX_PLAYERS, public: ROOM_PUBLIC});

room.setDefaultStadium("Big");
room.setScoreLimit(3);
room.setTimeLimit(3);
room.setCustomStadium(MAP);

// Bot variables
var admin_exist = false;
var global_players = [];



class PlayerInfo {

	constructor(id, name) {

		this.id = id;
		this.name = name;
		this.team = SPECTATOR;
		this.wins = 0;
		this.lost = 0;
		this.goals_score = 0;
		this.is_left = false;
	}
}

class Team {

	constructor() {

		this.games = 0;
		this.wins = 0;
		this.lost = 0;
		this.goals = 0;
		this.lost_goals = 0;
		this.players_id = new Array[0, 0, 0];
		this.winning_percentage = 0;
	}

	Reset() {

		this.games = 0;
		this.wins = 0;
		this.lost = 0;
		this.goals = 0;
		this.lost_goals = 0;
		this.players_id = new Array[0, 0, 0];
		this.winning_percentage = 0;
	}

	Win() {

		this.wins++;
		this.games++;

		if (this.wins == 0 && this.games == 0)
			this.winning_percentage = 0;
		else
			this.winning_percentage = this.wins / this.games;
	}

	Lost() {

		this.lost++;
		this.games++;

		if (this.wins == 0 && this.games == 0)
			this.winning_percentage = 0;
		else
			this.winning_percentage = this.wins / this.games;
	}

	AddScore(goals, lost_goals) {

		this.goals += goals;
		this.lost_goals += lost_goals;
	}

	SetPlayersID(players_id) {

		for (var i = 0; i < 3; i++)
			this.players_id[i] = players_id[i];

		// room.sendChat(String(Array.isArray(players_id)));
		// room.sendChat(String(players_id.length));
		// room.sendChat(String(players_id.toString()));

		// if (Array.isArray(players_id) && (players_id.length == 3)) {

		// 	this.player1_id = players_id[0];
		// 	this.player2_id = players_id[1];
		// 	this.player3_id = players_id[2];
		// }
	}

	TeamIsChanged(players) {

		for (var i = 0; i < 3; i++) {

			if (!((players[i].id == this.players_id[0]) || (players[i].id == this.players_id[1]) || (players[i].id == this.players_id[2])))
				return true;
		}

		return false;
	}
}


var red_team = new Team();
var blue_team = new Team();


function UpdateAdmin(player) { 

	if (player.name == ADMIN_NAME) {
  		
  		room.SetPlayersIDAdmin(player.id, true);
  		admin_exist = true;
	}
}

function WelcomePlayer(player) {

	room.sendChat("Hello " + player.name);
}

function UpdatePlayers() {

	var players = room.getPlayerList();

	for (var i = 0; i < players.length; i++) {

		var player = global_players.find(player => player.id == players[i].id)
		player.team = players[i].team;
	}
}

function UpdateTeamStatistic(score) {

	UpdatePlayers();

	var red_players = room.getPlayerList().filter((player) => player.team == RED_TEAM);
	var blue_players =  room.getPlayerList().filter((player) => player.team == BLUE_TEAM);

	if ((red_players.length != 3) || (blue_players.length != 3)) {
		
		red_team.Reset();
		blue_team.Reset();

		return;
	}

	if (red_team.TeamIsChanged(red_players)) {

		red_team.Reset();
		red_team.SetPlayersID(red_players);
	}
	
	if (blue_team.TeamIsChanged(blue_players)) {

		blue_team.Reset();
		blue_team.SetPlayersID(blue_players);

	}

	if (score.red > score.blue) {

		red_team.Win();
		red_team.AddScore(score.red, score.blue);

		blue_team.Lost();
		blue_team.AddScore(score.blue, score.red)

	}
	else {

		blue_team.Win();
		blue_team.AddScore(score.blue, score.red);
		red_team.Lost();
		red_team.AddScore(score.red, score.blue_team);
	}
}

function WriteRecordTeam(team) {


	//Red Statistic     Win/Lost => 4/2       GoalScore/GoalLost => 4/0               Ratio => 0.67

	if (((red_team.wins + red_team.lost) > 1) && ((blue_team.wins + blue_team.lost) > 1)) {

		room.sendChat("Red Stats W: " + red_team.wins 
					+ "  #  L: " + red_team.lost 
					+ "  #  GS: " + red_team.goals 
					+ "  #  GL: " + red_team.lost_goals 
					+ "  #  R: " + red_team.winning_percentage.toFixed(2));

		room.sendChat("Blue Stats W: " + blue_team.wins 
					+ "  #  L: " + blue_team.lost 
					+ "  #  GS: " + blue_team.goals 
					+ "  #  GL: " + blue_team.lost_goals 
					+ "  #  R: " + blue_team.winning_percentage.toFixed(2));
	}
	else if (team == RED_TEAM && red_team.games > 1) {

		room.sendChat("Red Stats W: " + red_team.wins 
					+ "  #  L: " + red_team.lost 
					+ "  #  GS: " + red_team.goals 
					+ "  #  GL: " + red_team.lost_goals 
					+ "  #  R: " + red_team.winning_percentage.toFixed(2));

	}
	else if (team == BLUE_TEAM && blue_team.games > 1) {

		room.sendChat("Blue Stats W: " + blue_team.wins 
					+ "  #  L: " + blue_team.lost 
					+ "  #  GS: " + blue_team.goals 
					+ "  #  GL: " + blue_team.lost_goals 
					+ "  #  R: " + blue_team.winning_percentage.toFixed(2));
	}

}

room.onPlayerJoin = function(player) {


	if (admin_exist && (player.name == ADMIN_NAME)) {

		room.kickPlayer(player.id, "Nie podszywaj sie pod admina", false);
	}
	else {

		global_players.push(new PlayerInfo(player.id, player.name));

		UpdateAdmin(player);
		WelcomePlayer(player);
	}
}

room.onPlayerLeave = function(player) {

	global_players.filter(plr => plr.id == player.id).is_left = true;
}

room.onTeamVictory = function(score) {

	UpdateTeamStatistic(score);

	if (score.red > score.blue)
		WriteRecordTeam(RED_TEAM);
	else
		WriteRecordTeam(BLUE_TEAM);
	}

}

window.onHBLoaded = main;